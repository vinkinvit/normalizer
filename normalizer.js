var curry = require('toolbox').curry;
var _ = require('highland');

module.exports = curry(function (type, data) {
  var normalizer = require('./entities/' + type + '.js');
  return new Promise(function (resolve, reject) {
    _(data)
      .through(normalizer())
      .collect()
      .each(function (normalized) {
        resolve(normalized);
      })
      .stopOnError(function (err) {
        reject(err);
      });
  });
});
