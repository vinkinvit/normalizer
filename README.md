Normalizer
=========

This project allow you to normalize the object format of a stream
of javascript objects. It exposes both a library and a command-line interface.

```bash
normalize --help
```

## Setup

Clone the repository and run `npm link` to make the executable available on the command line.


## Example usage

```bash
extract './ek_.xls' | normalize ek | jq
```
