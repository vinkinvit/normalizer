var yargs = require('yargs');

var argv = yargs
  .usage('$0 [options] <type>')
  .demand(1)
  .option('number', {
    type: 'number',
    alias: 'n',
    describe: 'The max number of objects to process.'
  })
  .help('help')
  .argv;

argv.type = argv._[0];

module.exports = argv;
