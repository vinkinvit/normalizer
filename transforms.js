'use strict';

var t = require('toolbox');

var normalize = t.curry(function (spec, obj) {
  return Object.keys(spec).reduce(function (agg, key) {
    return t.set(spec[key].trim(), obj[key] && obj[key].trim(), agg);
  }, {});
});

var makeDate = t.curry(function (props, obj) {
  props.forEach(function (key) {
    if (obj[key]) {
      obj[key] = new Date(obj[key]);
    }
  });
  return obj;
});

var convertMonth = t.curry(function (props, obj) {
  props.forEach(function (prop) {
    var months = {
      jan: '01', feb: '02', mar: '03', apr: '04',
      maj: '05', jun: '06', jul: '07', aug: '08',
      sep: '09', okt: '10', nov: '11', dec: '12'
    };
    obj[prop] = Object.keys(months).reduce(function (str, month) {
      return str.replace(new RegExp(month, 'gi'), months[month]);
    }, obj[prop]);
  });
  return obj;
});

var removeMillis = t.curry(function (props, obj) {
  props.forEach(function (prop) {
    obj[prop] = obj[prop].replace(/ \d{6}$/, '');
  });
  return obj;
});

var ssnr = function ssnr(obj) {
  if (obj && obj.ssnr) {
    obj.ssnr = obj.ssnr.match(/\d+/gi).join('');

    if (obj.ssnr.length === 10) {
      // Assume person is born in the 20'th century.
      obj.ssnr = '19' + obj.ssnr;
    }
  };

  return obj;
};

var hsa = function hsa(obj) {
  if (obj && obj.hsa && obj.hsa.length <= 4) {
    obj.hsa = 'SE2321000016-' + obj.hsa.toUpperCase();
  }
  return obj;
};

var karolinska = t.lax(function (x) {
  return x.kombika.match(/^1100(1|2|3)/);
}, true);

var nonClosed = t.lax(function (x) {
  return new Date() < new Date(x.end);
}, true);

module.exports = {
  normalize: normalize,
  makeDate: makeDate,
  convertMonth: convertMonth,
  removeMillis: removeMillis,
  ssnr: ssnr,
  hsa: hsa,
  karolinska: karolinska,
  nonClosed: nonClosed
};
