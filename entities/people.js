var _ = require('highland');
var conformer = require('../conformer.js');

var spec = {
  "ssnr": "ssnr",
  "hsa": "hsa"
}

module.exports = function () {
  return _.pipeline(
    _.through(conformer(spec))
  );
}
