var _ = require('highland');
var conformer = require('../conformer.js');

var spec = {
  "Id": "kombika",
  "Förkortning": "abbreviation",
  "Långt namn": "nameLong",
  "Startdatum": "start",
  "Slutdatum": "end",
  "Postadress": "address",
  "Besöksadress": "visitingAddress",
  "Postnummer": "zipcode",
  "Ort": "city",
  "Telefonnummer": "phone",
  "Internadress": "internalAddress"
}

module.exports = function () {
  return _.pipeline(
    _.through(conformer(spec))
  );
};
