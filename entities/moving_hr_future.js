var _ = require('highland');
var conformer = require('../conformer.js');

var spec = {
  "PERSONNR": "ssnr",
  "NAMN": "name",
  "KONTERING": "fromCostcenter",
  "DIV": "division",
  "KLIN_AVD": "clinic",
  "SEKTION": "section",
};

module.exports = function () {
  return _.pipeline(
    _.through(conformer(spec)),
    _.map(function(obj) {
      var names = obj.name.match(/[^\s]+/gi);
      delete obj.name;
      obj.lastname = names[names.length-1];
      obj.firstname = names.slice(0, -1).join(' ');
      return obj;
    })
  );
}
