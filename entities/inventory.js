var _ = require('highland');
var conformer = require('../conformer.js');
var lax = require('toolbox').lax;

var spec = {
  'Flyttvåg': 'moveWave',
  'Nuvarande lokalnummer': 'fromRoomNr',
  'Flyttas till': 'toRoomNr',
  'Inventarie': 'item',
  'Flyttdatum': 'moveDate',
  'Division': 'division',
  'Verksamhet': 'section',
  'Avdelning': 'unit',
  'Kostnadsställe': 'costcenter'
}

module.exports = function () {
  return _.pipeline(
    _.through(conformer(spec)),
    _.reject(lax(function(x) {
      return x.toRoomNr.match(/(ej till NKS)/g);
    }))
  );
}
