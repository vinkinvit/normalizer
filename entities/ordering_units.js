var _ = require('highland');
var conformer = require('../conformer.js');

var spec = {
  "Best enhet": "orderingId",
  "Verksamhet (tema/funktion)": "division",
  "Avdelningsnamn": "unit",
  "HSA-ID": "hsa",
  "Beställarenhet, namn i CW": "orderingCWName",
  "Site": "locality",
  "Kostnadsställe": "costcenter",
  "BIM-kod: AGV stn": "agvStation",
  "BIM: Postfacksnisch": "postBox",
  "Internadress NKS": "internalAdress",
  "Inflyttningsdatum, NKS": "moveDate"
};

module.exports = function () {
  return _.pipeline(
    _.through(conformer(spec))
  );
}
