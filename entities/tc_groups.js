var _ = require('highland');
var conformer = require('../conformer.js');

var spec = {
  hsaid: 'hsa',
  anvandare_namn_tc: 'user',
  vardenhetsgrupp_id: 'groupId',
  vardenhetsgrupp_namn: 'group',
  giltig_tom: 'validTo',
  senast_inloggad: 'lastestLogin',
  inaktiveringstidpunkt: 'inactiveAt'
}

module.exports = function () {
  return _.pipeline(
    _.through(conformer(spec))
  );
};

