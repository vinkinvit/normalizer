var _ = require('highland');
var conformer = require('../conformer.js');

var spec = {
  "Tema/funktions ID": "divisionId",
  "Tema/Funktion": "division",
  "PO/FO ID": "clinicId",
  "PO/FO/Staber…": "clinic",
  "PE/FE ID": "sectionId",
  "FE/PF/…": "section",
  "KST": "costcenter",
  "Kostnadsställenamn (EK - max 146 tecken)": "nameEk",
  "Kostnadsställenamn (Raindance - max 30 tecken)": "nameRaindance"
}

module.exports = function () {
return _.pipeline(
    _.through(conformer(spec))
  );
}
