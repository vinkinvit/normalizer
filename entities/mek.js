var _ = require('highland');
var conformer = require('../conformer.js');
var t = require('../transforms.js');

var spec = {
  'Kombikakod': 'kombika',
  'HSAId': 'hsa',
  'FromDatum': 'starts',
  'TillDatum': 'ends',
  'SkapadDatum': 'made'
}

module.exports = function () {
  return _.pipeline(
    _.through(conformer(spec)),
    _.map(t.makeDate(['starts', 'ends', 'made'])),
    _.filter(t.karolinska)
  );
}
