var _ = require('highland');
var conformer = require('../conformer.js');
var t = require('../transforms.js');

var spec = {
  "Id": "kombika",
  "Förkortning": "abbreviation",
  "Kort namn": "nameShort",
  "Långt namn": "nameLong",
  "Hierarkisk nivå": "hierarcy",
  "Startdatum": "start",
  "Slutdatum": "end",
  "Senast ändrat datum": "modifiedAt",
  "Senast ändrad av": "modifiedBy",
  "Status": "status",
  "Skapad": "createdAt"
}

module.exports = function () {
  return _.pipeline(
    _.through(conformer(spec)),
    _.map(t.makeDate(['start', 'end', 'modifiedAt', 'createdAt'])),
    _.filter(t.nonClosed)
  );
}
