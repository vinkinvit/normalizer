var _ = require('highland');
var conformer = require('../conformer.js');
var get = require('toolbox').get;

var spec = {
  'Startdatum': 'start',
  'Kombikakod': 'kombika',
  'Förkortning': 'abbreviation',
  'Kortnamn': 'nameShort',
  'Långt namn': 'nameLong',
  'Specialitet': 'speciality',
  'Avdelning': 'wardType',
  'Kostnadsställe': 'costcenter',
  'HSA-id': 'hsa',
}

module.exports = function () {
  return _.pipeline(
    _.through(conformer(spec)),
    _.filter(get('kombika')),
    _.tap(function(x) {
      x.kombika = x.kombika.match(/(\w|\d)+/g).join('');
    })
  );
}
