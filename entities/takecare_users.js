var _ = require('highland');
var conformer = require('../conformer.js');

var spec = {
  "namn": "name",
  "hsaid": "hsa",
  "giltig_tom": "validTo",
  "inaktiveringstidpunkt": "inactiveAt",
  "senast_inloggad": "lastestLogin"
}

module.exports = function () {
  return _.pipeline(
    _.through(conformer(spec))
  );
};
