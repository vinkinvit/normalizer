var _ = require('highland');
var conformer = require('../conformer.js');
var t = require('../transforms.js');

var spec = {
  "vardenhet_id": "careunitId",
  "namn": "name",
  "adress_rad1": "addressRow1",
  "adress_rad2": "addressRow2",
  "adress_rad3": "addressRow3",
  "doman_id": "domainId",
  "postnummer": "postnumber",
  "postort": "city",
  "lan_id": "lanId",
  "telefon_intern": "internalPhone",
  "telefon_extern": "externalPhone",
  "fax": "fax",
  "kombika": "kombika",
  "EAN": "ean",
  "plusgiro": "plusgiro",
  "IsOpenFiveDaysAWeek": "openFiveDaysAWeek",
  "sangplatser": "nrBeds",
  "koordinerad_vardplanering": "coordinatedCarePlanning",
  "e_koordinerad_vardplanering": "ekCoordinatedCarePlanning",
  "labresultatmottagare": "labResultRecipient",
  "giltig_tom": "validThrough",
  "senast_uppdaterad": "lastModified",
  "bankgiro": "bankgiro",
  "foretag_id": "companyId",
  "arbetsplats_kod": "workCode",
  "elektronisk_dos_dispenser": "electronicDoseDispenser",
  "dos_dispens_giltig_tom": "doseDispenserValidThrough",
  "dos_dispens_forsta_anvandning": "doseDispenserFirstUse",
  "dos_dispens_apotek_id": "doseDispenserPharmacyId",
  "katz_matt_mall_id": "katzMattTemplateId",
  "katz_term_id": "katzTermId",
  "HSAID": "hsa",
  "skyddad_enhet": "protectedUnit",
  "foretag_kod": "companyCode",
  "anvander_diagnoser_signeringslista": "usesDiagnoseSigningList"
}

module.exports = function () {
  return _.pipeline(
    _.through(conformer(spec)),
    _.map(t.makeDate(['doseDispenserValidThrough', 'doseDispenserFirstUse', 'validThrough', 'lastModified'])),
    _.tap(function(x) {
      Object.keys(x).forEach(function (key) {
        if (x[key] === 'NULL') {
          x[key] = undefined;
        }
      })
    }),
    _.tap(function(x) {
      var booleans = [ 'domainId', 'lanId', 'openFiveDaysAWeek', 'coordinatedCarePlanning', 'ekCoordinatedCarePlanning', 'labResultRecipient', 'electronicDoseDispenser', 'usesDiagnoseSigningList', 'protectedUnit', 'intensiveCare' ];
      booleans.forEach(function(key) {
        if (x[key] != undefined) {
          x[key] = x[key] === "1";
        }
      });
    }),
    // _.tap(function(x) {
    //   x.closed = !!x.name.match(/stängd/i)}
    // ),
    _.filter(t.karolinska)
  );
}
