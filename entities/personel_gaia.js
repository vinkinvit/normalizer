var _ = require('highland');
var conformer = require('../conformer.js');

var spec = {
  "GroupName": "group",
  "GroupHsaIdentity": "groupHsa",
  "UserName": "name",
  "UserHsaIdentity": "hsa",
  "Enabled": "enabled"
};

module.exports = function () {
  return _.pipeline(
    _.through(conformer(spec)),
    _.map(function(obj) {
      obj.enabled = obj.enabled === 'TRUE';
      return obj;
    }),
    _.map(function(obj) {
      var names = obj.name.match(/[^\s\(\)]+/g).slice(0, -1);
      obj.firstname = names.slice(0, -1).join(' ');
      obj.lastname = names[names.length-1]
      delete obj.name;
      return obj;
    })
  );
}
