var _ = require('highland');
var conformer = require('../conformer.js');
var t = require('../transforms.js');

var spec = {
  "KONTODEL2_REF": "ref",
  "BEN": "name",
  "Arbetsställe (H/S)": "locality",
  "KONTERING": "costcenter",
  "FOM": "validFrom",
  "FOERETAG": "company",
  "STATUS_KOD": "statusCode",
  "AENDR_USER": "modifiedBy",
  "AENDR_TIDP": "modifiedAt",
  "PROD_TIDP": "validProduction",
  "TEST_TIDP": "validTest"
}

module.exports = function () {
  return _.pipeline(
    _.through(conformer(spec)),
    _.map(t.convertMonth(['validFrom', 'modifiedAt', 'validProduction', 'validTest'])),
    _.map(t.removeMillis(['modifiedAt', 'validProduction', 'validTest'])),
    _.map(t.makeDate(['validFrom', 'modifiedAt', 'validProduction', 'validTest']))
  );
}
