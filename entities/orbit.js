var _ = require('highland');
var conformer = require('../conformer.js');

var spec = {
  "PO/FO": "division",
  "Avd": "unit",
  "Kombika": "kombika",
  "Betalande enhet": "paying",
  "Betalande enhet anestesi-konsultation": "payingAnestethicsConsultation",
  "Uppvak": "postopUnit",
  "Producerande anestesi": "producingAnestetics",
  "Producerande operation": "producingOperation",
  "Producerande enhet anestesi-konsultation": "producingAnesteticsConsultation"
};



module.exports = function () {
  return _.pipeline(
    _.through(conformer(spec)),
    _.tap(function (x) {
      x.paying = x.paying == 1;
      x.payingAnestethicsConsultation = x.payingAnestethicsConsultation == 1;
      x.producingOperation = x.producingOperation == 1;
      x.producingAnestetics = x.producingAnestetics == 1;
      x.producingAnesteticsConsultation = x.producingAnesteticsConsultation == 1;
    })
  );
}
