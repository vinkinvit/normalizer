var _ = require('highland');
var conformer = require('../conformer.js');

var spec = {
  "sAMAccountName": "group",
  "hsaIdentity": "hsa",
  "sllEkDn": "orgpath",
  "Level": "level"
}

module.exports = function () {
  return _.pipeline(
    _.through(conformer(spec)),
    _.tap(function(x) {
      x.orgpath = x.orgpath.replace(/; /gi, ', ')
    })
  );
}
