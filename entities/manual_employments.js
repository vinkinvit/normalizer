var _ = require('highland');
var conformer = require('../conformer.js');

var spec = {
  "Dn": "path",
  "Efternamn": "lastname",
  "HSA-id": "hsa",
  "Organisations-ID": "costcenter",
  "Personnummer": "ssnr",
  "Tilltalsnamn": "firstname"
}

module.exports = function () {
  return _.pipeline(
    _.through(conformer(spec)),
    _.tap(function(obj) {
      if (obj.costcenter) {
        obj.costcenter = obj.costcenter.match(/\d+/).join()
      }
    })
  );
};
