var _ = require('highland');
var conformer = require('../conformer.js');

var spec = {
  'ID': 'nid',
  'HSA-ID': 'hsa',
  'Tema/Funktion': 'division',
  'Patomr/Funkomr': 'clinic',
  'Patgrupp/funkenh': 'section',
  'Avd namn': 'unit',
  'Kst': 'costcenter',
  'Ek-funktion': 'subunit',
  'Ref gammal': 'old',
  'Huskropp': 'house',
  'Plan': 'level'
};

module.exports = function () {
  return _.pipeline(
    _.through(conformer(spec))
  );
}
