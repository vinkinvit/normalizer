var _ = require('highland');
var conformer = require('../conformer.js');
var t = require('../transforms.js');

var spec = {
  "div_id": "divisonId",
  "div_text": "division",
  "klinik_id": "clinicId",
  "klinik_text": "clinic",
  "sekt_id": "sectionId",
  "sekt_text": "section",
  "kst_id": "costcenter",
  "kst_text": "name",
}

module.exports = function () {
  return _.pipeline(
    _.through(conformer(spec))
  );
}
