var _ = require('highland');
var conformer = require('../conformer.js');

var spec = {
  "Namn": "name",
  "HSAID": "hsa",
  "Yrkesgrupp": "workgroup",
  "Grupp": "group",
  "Enhet": "unit",
  "Anställd vid": "employedAt",
  "Behörighetvia": "permissionBoard"
}

module.exports = function () {
  return _.pipeline(
    _.through(conformer(spec)),
    _.tap(x => x.hsa = `SE2321000016-${x.hsa}`)
  );
};
