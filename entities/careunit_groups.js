var _ = require('highland');
var conformer = require('../conformer.js');

var spec = {
  vardenhetsgrupp_id: 'careunitGroupId',
  vardenhetsgrupp_namn: 'careunitGroupName',
  vardenhet_id: 'careunitId',
  vardenhet_namn: 'careunitName',
  kombika: 'kombika'
}

module.exports = function () {
  return _.pipeline(
    _.through(conformer(spec))
  );
}
