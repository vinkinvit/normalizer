var _ = require('highland');
var moment = require('moment-timezone');
var conformer = require('../conformer.js');

var spec = {
  "Dn": "path",
  "Accessroller": "accessrights",
  "HSA-id": "hsa",
  "Personnummer": "ssnr",
  "Efternamn": "lastname",
  "Tilltalsnamn": "firstname",
  "Startdatum": "start",
  "Slutdatum": "end",
  "Organisations-ID": "costcenter",
  "Telefonnummer": "phone",
  "Titel/Befattning": "title"
};

module.exports = function () {
  return _.pipeline(
    _.through(conformer(spec)),
    _.tap(function(obj) {
      if (obj.costcenter) {
        obj.costcenter = obj.costcenter.match(/\d+/).join()
      }
    }),
    _.tap(function(obj) {
      if (obj.start) {
        obj.start = moment.tz(obj.start, 'Europe/Stockholm').toDate().toISOString();
      }
      if (obj.end) {
        obj.end = moment.tz(obj.end, 'Europe/Stockholm').toDate().toISOString();
      }
    })
    // ,
    // _.filter(function(obj) {
    //   if (!(!obj.end || obj.end >= new Date().toISOString())) {
    //     console.log(obj.end, new Date().toISOString());
    //   }
    //   return !obj.end || obj.end >= new Date().toISOString()
    // })
  );
}
