var _ = require('highland');
var conformer = require('../conformer.js');
var t = require('toolbox');

var spec = {
  'Dn' : 'orgpath',
  'Besöksadress' : 'visitingaddress',
  'Enhetsnamn' : 'name',
  'HSA-id' : 'hsa',
  'Lokalitet' : 'locality',
  'Organisations-ID' : 'costcenters',
  'Postadress' : 'postaddress',
  'Telefonnummer' : 'phone',
  'Tjänstefax' : 'fax',
  'Kortnamn': 'shortname'
}

module.exports = function () {
  return _.pipeline(
    _.through(conformer(spec)),
    _.tap(t.lax(function(x) {
      x.costcenters = x.costcenters.match(/\d+/g), [];
    })),
    _.tap(function(x) {
      var orgs = t.init(x.orgpath.split(', ')).reverse();
      x.hospital = orgs[0];
      x.division = orgs[1];
      x.clinic = orgs[2];
      x.section = orgs[3];
      x.unit = orgs[4];
      x.subunit = orgs[5];
      x.orgpath = orgs.join(', ');
    })
  );
}
