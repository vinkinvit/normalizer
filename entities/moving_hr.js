var _ = require('highland');
var conformer = require('../conformer.js');

var spec = {
  "Pnr": "ssnr",
  "Firstname": "firstname",
  "Lastname": "lastname",
  "Division": "division",
  "Klinik": "clinic",
  "Sektion": "section",
  "Tidigare kst": "fromCostcenter",
  "Tema/funktion": "toDivision",
  "Kostnadsställe, kod": "toCostcenter",
  "Verifikationsdatum": "verificationDate",
}

module.exports = function () {
  return _.pipeline(
    _.through(conformer(spec)),
    _.filter(x => !x.verificationDate),
    _.tap(x => {
      delete x.verificationDate
    })
  );
};
