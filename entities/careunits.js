var _ = require('highland');
var conformer = require('../conformer.js');
var t = require('../transforms.js');

var spec = {
  "Flik": "division",
  "Kombikakod": "kombika",
  "Namn (infört i TC)": "name",
  "Gammalt namn": "oldName",
  "Startdatum": "start",
  "Vårdenhets-ID": "careunitId",
  "HSA-ID (Infört i TC)": "hsa",
  "APK (Infört i TC)": "apk",
  "adress_rad1": "adress1",
  "adress_rad2": "adress2",
  "adress_rad3": "adress3",
  "postnummer": "zipcode",
  "postort": "city",
  "Adress": "adress",
  "Telefon&FAX": "phone",
  "Vårdplatser": "beds",
  "Mottagare konsultationsremiss": "receiverReferrals",
  "Spärrgrupp": "latchgroup",
  "VIP": "vip",
  "VE svarsmottagare av labsvar": "answerReceiver",
  "VE ha papperssvar i tillägg till e-svar": "paperAnswer",
  "Ekonomiska kombikor inkl namn": "economicKombikas",
  "Tidigare vårdenheter som blir förändrade/slås ihop": "previousCareunits",
  "Webbtidbok": "webbooking",
  "Enhet i CCC (Clinisoft)": "unitInCCC",
  "ApoDos": "apodos",
  "Lab beställt": "labOrdered",
  "P & U Beställt": "puOrdered",
  "Konfigurerad för Ris/Pacs": "configureForRP",
  "Konfigurerad för Picsara": "configureForPic",
  "Konfigurerad för CCC": "configureForCCC",
  "Kommentar": "comment",
  "Kommentar 1 Versamhetens önskemål om namn": "comment1",
  "Kommentar 2": "comment2",
  "Kommentar 3": "comment3"
}

module.exports = function () {
  return _.pipeline(
    _.through(conformer(spec)),
    _.filter(obj => obj.kombika),
    _.tap(function(x) {
      x.kombika = x.kombika.match(/(\w|\d)+/g).join('');
    }),
    _.tap(function(x) {
      if (x.economicKombikas) {
        var kombs = x.economicKombikas.match(/\d{5}-\d{3}-\w{3}/gi) || [];
        x.economicKombikas = kombs.map(function (kombika) {
          return kombika.match(/\w+/gi).join('')
        })
      }
    })
  );
}
