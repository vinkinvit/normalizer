var _ = require('highland');
var moment = require('moment-timezone');
var conformer = require('../conformer.js');

module.exports = function () {

  var months = {
    jan: 1,
    feb: 2,
    mar: 3,
    apr: 4,
    maj: 5,
    jun: 6,
    jul: 7,
    aug: 8,
    sep: 9,
    okt: 10,
    nov: 11,
    dec: 12
  };

  var spec = {
    "PERSONNR": "ssnr",
    "IDNR": "heromaId",
    "FOM": "validFrom",
    "TOM": "validTo",
    "KONTERING": "costcenter"
  }

  const makeDate = datestring => {
    if (!datestring) { return datestring; }

    var dateParts = datestring.split('-');

    var year = dateParts[0],
        month = dateParts[1],
        day = dateParts[2];

    month = months[month];

    return moment.tz(year + '-' + month + '-' + day, 'Europe/Stockholm').toDate().toISOString();
  }

  return _.pipeline(
    _.through(conformer(spec)),
    _.tap(function(obj) {
      if (obj.costcenter) {
        obj.costcenter = obj.costcenter.match(/\d+/).join()
      }
    }),
    _.tap(function(obj) {
      obj.validFrom = makeDate(obj.validFrom);
      obj.validTo = makeDate(obj.validTo);
    })
  );
}
