var _ = require('highland');
var conformer = require('../conformer.js');

var spec = {
  "hsaid": "hsa",
  "anvandare_namn": "user",
  "vardenhet_id": "unitId",
  "vardenhet_namn": "unit",
  "giltig_tom": "validTo",
  "senast_inloggad": "lastestLogin",
  "inaktiveringstidpunkt": "inactiveAt"
}

module.exports = function () {
  return _.pipeline(
    _.through(conformer(spec))
  );
};
