var _ = require('highland');
var conformer = require('../conformer.js');

var spec = {
  "Chef": "boss",
  "Befintligt PO/FO": "currentPOFO",
  "Nytt Kostnadsställe": "newCostcenter"
}

module.exports = function () {
  return _.pipeline(
    _.through(conformer(spec)),
    _.tap(x => {
      x.newCostcenter = x.newCostcenter.match(/\d+/gi)
    })
  );
};
