var _ = require('highland');
var conformer = require('../conformer.js');
var get = require('toolbox').get;

var spec = {
  "Kombikakod, nummer": "kombika",
  "Tema/Funktion": "division",
  "Site": "site",
  "Långt namn": "nameLong",
  "Namn på PO eller FO som kombikan tillhör": "unitname",
  "Betalande enhet": "paying",
  "Producerande anestesi": "producingAnestethics",
  "Producerande operation": "producingOperation",
  "Betalande Anestesi-\r\nkonsultation": "payingAnestethicsConsultation",
  "Producerande Anestesi-\r\nkonsultation": "producingAnesteticsConsultation",
  "Postop/\r\nUppvakningsenhet": "postopUnit",
  "Operationsavdelning\r\n(Opererar på)": "operatingUnit",
  "Primära vårdeneheter i TakeCare": "primaryCareUnitsInTC",
  "Lokalitet för vårdenhet i TakeCare": "localityForCareunitInTC",
  "Förslag på vårdande enhet för H/K i Huddinge och Solna från verksamhet": "careUnitForHK",
  "Kommentarer": "comments",
  "Orbit-förvaltningens kommentar": "orbitComment"
}

module.exports = function () {
  return _.pipeline(
    _.through(conformer(spec)),
    _.tap(function(x) {
      x.kombika = x.kombika.match(/(\w|\d)+/g).join('');
    }),
    _.tap(function (x) {
      x.paying = !!x.paying;
      x.producingAnestethics = !!x.producingAnestethics;
      x.producingOperation = !!x.producingOperation;
      x.payingAnestethicsConsultation = !!x.payingAnestethicsConsultation;
      x.producingAnesteticsConsultation = !!x.producingAnesteticsConsultation;
    })
  );
};
