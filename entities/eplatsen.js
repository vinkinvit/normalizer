var _ = require('highland');
var conformer = require('../conformer.js');

var spec = {};

module.exports = function () {
  return _.pipeline(
    _.through(conformer(spec))
  );
}
