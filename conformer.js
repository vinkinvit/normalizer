var _ = require('highland');
var t = require('./transforms.js');

module.exports = function (spec) {
  return _.pipeline(
    _.map(t.normalize(spec)),
    _.map(t.hsa),
    _.map(t.ssnr)
  );
}
