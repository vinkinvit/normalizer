#!/usr/bin/env node

var _ = require('highland');
var chalk = require('chalk');
var id = require('toolbox').id;

var argv = require('./cli.args.js');
var normalize = require('./normalizer.js');


function lines(s) {
  return s + '\n';
};

var input = _(process.stdin);
var objStream = input.split().filter(id).map(JSON.parse);

var normalized = objStream.collect().map(function (data) {
  return _(normalize(argv.type, data));
}).flatten();

var limited = (argv.number) ? normalized.take(argv.number) : normalized;
var stringified = limited.map(JSON.stringify).map(lines);
var safe = stringified.stopOnError(function (err) {
  console.error(chalk.red(err.stack))
});

// output
safe.pipe(process.stdout);
